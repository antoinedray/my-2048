﻿using System;

namespace Application
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Grid run = new Grid();
            Controller control = new Controller();

            // Put the first two values in the game
            run.PlaceRandom();
            run.PlaceRandom();
            run.Display();

            while (true)
            {
                if(run.Move(control.GetAction()))
                {
                    run.PlaceRandom();
				}
                Console.Clear();
                run.Display();
            }
        }
    }
}
