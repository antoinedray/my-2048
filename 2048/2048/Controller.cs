﻿using System;

namespace Application
{
    public class Controller
    {
        public Controller()
        {
        }

        public enum Action { Up, Down, Left, Right, Quit };

        public Action GetAction()
        {
            Action result = Action.Up;
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.UpArrow:
                    result = Action.Up;
                    break;
                case ConsoleKey.DownArrow:
					result = Action.Down;
					break;
                case ConsoleKey.LeftArrow:
                    result = Action.Left;
					break;
                case ConsoleKey.RightArrow:
                    result = Action.Right;
					break;
                case ConsoleKey.Escape:
                    result = Action.Quit;
					break;
                default:
                    break;
            }
            return result;
        }
    }
}
