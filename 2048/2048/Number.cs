﻿using System;
using System.Collections.Generic;

namespace Application
{
    public class Number
    {
        public Dictionary<int, ConsoleColor> colors = new Dictionary<int, ConsoleColor>();

        public Number()
        {
			// Colors setup
			colors.Add(0, ConsoleColor.Black);
			colors.Add(2, ConsoleColor.Gray);
			colors.Add(4, ConsoleColor.DarkGray);
			colors.Add(8, ConsoleColor.Blue);
			colors.Add(16, ConsoleColor.DarkBlue);
			colors.Add(32, ConsoleColor.Yellow);
			colors.Add(64, ConsoleColor.DarkYellow);
			colors.Add(128, ConsoleColor.Green);
			colors.Add(256, ConsoleColor.DarkGreen);
			colors.Add(512, ConsoleColor.Cyan);
			colors.Add(1024, ConsoleColor.DarkCyan);
			colors.Add(2048, ConsoleColor.Red);
			colors.Add(4096, ConsoleColor.DarkRed);
			colors.Add(8192, ConsoleColor.Magenta);
			colors.Add(16384, ConsoleColor.DarkMagenta);
        }
    }
}
