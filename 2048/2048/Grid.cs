﻿using System;
using System.Collections.Generic;

namespace Application
{
    public class Grid
    {
		private int[,] grid;
        Random rand = new Random();
        Number color = new Number();

        public Grid()
        {
			// We initialize the grid 4x4 ; (x,y)
			grid = new int[4, 4];

			// We initialize the map as empty (Full of 0)
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					grid[i, j] = 0;
        }

		public bool Move(Controller.Action move)
		{
            bool hasMoved = false;

			switch (move)
			{
				case Controller.Action.Right:
					for (int i = 0; i < 4; i++)
					{
						for (int j = 2; j >= 0; j--)
						{
							if (grid[i, j] > 0)
							{
                                int k = 1;
                                while (j + k < 4 && grid[i, j + k] == 0)
                                {
									grid[i, j + k] = grid[i, j + k - 1];
									grid[i, j + k - 1] = 0;
                                    hasMoved = true;
                                    k++;
                                }
							}
						}
                        for (int j = 2; j >= 0; j--)
                        {
                            if (grid[i, j] > 0)
                            {
                                if (grid[i, j] == grid[i, j + 1])
                                {
                                    grid[i, j + 1] *= 2;
                                    Score.score += grid[i, j + 1];
                                    grid[i, j] = 0;
                                    hasMoved = true;
                                }
                            }
                        }
						for (int j = 2; j >= 0; j--)
						{
							if (grid[i, j] > 0)
							{
								int k = 1;
								while (j + k < 4 && grid[i, j + k] == 0)
								{
									grid[i, j + k] = grid[i, j + k - 1];
									grid[i, j + k - 1] = 0;
                                    hasMoved = true;
									k++;
								}
                            }
                        }
					}
					break;
                case Controller.Action.Left:
					for (int i = 0; i < 4; i++)
					{
						for (int j = 1; j < 4; j++)
						{
							if (grid[i, j] > 0)
							{
								int k = 1;
								while (j - k >= 0 && grid[i, j - k] == 0)
								{
									grid[i, j - k] = grid[i, j - k + 1];
									grid[i, j - k + 1] = 0;
                                    hasMoved = true;
                                    k++;
								}
							}
						}
                        for (int j = 1; j < 4; j++)
                        {
                            if (grid[i, j] > 0)
                            {
                                if (grid[i, j] == grid[i, j - 1])
                                {
                                    grid[i, j - 1] *= 2;
                                    Score.score += grid[i, j - 1];
                                    grid[i, j] = 0;
                                    hasMoved = true;
                                }
                            }
                        }
						for (int j = 1; j < 4; j++)
						{
							if (grid[i, j] > 0)
							{
								int k = 1;
								while (j - k >= 0 && grid[i, j - k] == 0)
								{
									grid[i, j - k] = grid[i, j - k + 1];
									grid[i, j - k + 1] = 0;
                                    hasMoved = true;
									k++;
								}
                            }
                        }
					}
					break;
				case Controller.Action.Up:
					for (int j = 0; j < 4; j++)
					{
						for (int i = 1; i < 4; i++)
						{
							if (grid[i, j] > 0)
							{
								int k = 1;
								while (i - k >= 0 && grid[i - k, j] == 0)
								{
									grid[i - k, j] = grid[i - k + 1, j];
									grid[i - k + 1, j] = 0;
                                    hasMoved = true;
									k++;
								}
							}
						}
                        for (int i = 1; i < 4; i++)
                        {
                            if (grid[i, j] > 0)
                            {
                                if (grid[i, j] == grid[i - 1, j])
                                {
                                    grid[i - 1, j] *= 2;
                                    Score.score += grid[i - 1, j];
                                    grid[i, j] = 0;
                                    hasMoved = true;
                                }
                            }
                        }
						for (int i = 1; i < 4; i++)
						{
							if (grid[i, j] > 0)
							{
								int k = 1;
								while (i - k >= 0 && grid[i - k, j] == 0)
								{
									grid[i - k, j] = grid[i - k + 1, j];
									grid[i - k + 1, j] = 0;
                                    hasMoved = true;
									k++;
								}
                            }
                        }
					}
					break;
				case Controller.Action.Down:
					for (int j = 0; j < 4; j++)
					{
						for (int i = 2; i >= 0; i--)
						{
							if (grid[i, j] > 0)
							{
								int k = 1;
								while (i + k < 4 && grid[i + k, j] == 0)
								{
									grid[i + k, j] = grid[i + k - 1, j];
									grid[i + k - 1, j] = 0;
                                    hasMoved = true;
									k++;
								}
							}
						}
                        for (int i = 2; i >= 0; i--)
                        {
                            if (grid[i, j] > 0)
                            {
                                if (grid[i, j] == grid[i + 1, j])
                                {
                                    grid[i + 1, j] *= 2;
                                    Score.score += grid[i + 1, j];
                                    grid[i, j] = 0;
                                    hasMoved = true;
                                }
                            }
                        }
						for (int i = 2; i >= 0; i--)
						{
							if (grid[i, j] > 0)
							{
								int k = 1;
								while (i + k < 4 && grid[i + k, j] == 0)
								{
									grid[i + k, j] = grid[i + k - 1, j];
									grid[i + k - 1, j] = 0;
                                    hasMoved = true;
									k++;
								}
                            }
                        }
					}
					break;
				case Controller.Action.Quit:
                    Environment.Exit(0);
                    break;
				default:
					break;
			}
            return hasMoved;
		}

        public void PlaceRandom()
        {
            bool isnOK = true;
            while (isnOK)
            {
				int x = rand.Next(0, 4);
				int y = rand.Next(0, 4);
                if (grid[x, y] == 0)
                {
                    grid[x, y] = rand.Next(1, 2) * 2;
                    isnOK = false;
                }
            }
        }

		public void Display()
		{
            bool success = false;

			for (int i = 0; i < 4; i++)
			{
                Console.BackgroundColor = ConsoleColor.White;
				Console.Write("                             ");
                Console.ResetColor();
                Console.Write("\n");

				for (int k = 0; k < 4; k++)
				{
					Console.BackgroundColor = ConsoleColor.White;
					Console.Write(" ");
					Console.ResetColor();
					Console.BackgroundColor = color.colors[grid[i, k]];
					Console.Write("      ");
					Console.ResetColor();
				}
				Console.BackgroundColor = ConsoleColor.White;
				Console.Write(" ");
				Console.ResetColor();
				Console.Write("\n");

				for (int j = 0; j < 4; j++)
				{

					Console.BackgroundColor = ConsoleColor.White;
					Console.Write(" ");
					Console.ResetColor();

                    if (grid[i, j] == 0)
                        Console.Write("      ");
                    else
                    {
                        if (grid[i,j] >= 2048)
                        {
                            success = true;
                        }
						Console.BackgroundColor = color.colors[grid[i, j]];
						Console.Write(String.Format("{0,-6}", String.Format("{0," + ((6 + grid[i, j].ToString().Length) / 2).ToString() + "}", grid[i, j].ToString())));
						Console.ResetColor();
                    }
				}

                Console.BackgroundColor = ConsoleColor.White;
				Console.Write(" ");
                Console.ResetColor();
                Console.Write("\n");

				for (int k = 0; k < 4; k++)
				{
					Console.BackgroundColor = ConsoleColor.White;
					Console.Write(" ");
					Console.ResetColor();
					Console.BackgroundColor = color.colors[grid[i, k]];
					Console.Write("      ");
					Console.ResetColor();
				}
				Console.BackgroundColor = ConsoleColor.White;
				Console.Write(" ");
				Console.ResetColor();
				Console.Write("\n");
			}

			Console.BackgroundColor = ConsoleColor.White;
			Console.Write("                             ");
			Console.ResetColor();
            Console.Write("\n");

            Console.Write("\n");
            Console.WriteLine("Score : " + Score.score);

            if (success)
            {
				Console.Write("\n");
                Console.BackgroundColor = ConsoleColor.Green;
				Console.WriteLine("You reach 2048, you win !!!");
                Console.ResetColor();
            }
		}
    }
}
